package Exercise.data.owners;

import Exercise.data.vehicles.cars.Car;

import java.util.ArrayList;

public class Owner {
    private ArrayList<Car> ownerCarList = new ArrayList<>();
    private String name;
    private String surname;

    public Owner(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public void buyCar(Car car) {
        this.ownerCarList.add(car);
        car.setOwner(this);
    }

    public ArrayList<Car> getOwnerCarList() {
        return ownerCarList;
    }

    public void setOwnerCarList(ArrayList<Car> ownerCarList) {
        this.ownerCarList = ownerCarList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void printCarList() {
        System.out.println(name + " owns: ");
        ownerCarList.stream()
                .map(car -> car.getBrand() + " " + car.getModel().getModelName() + " " + car.getModel().getModelVariant())
                .forEach(System.out::println);
    }

    @Override
    public String toString() {
        return name + " " + surname;
    }
}

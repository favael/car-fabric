package Exercise.data.vehicles;

import java.util.ArrayList;
import java.util.List;

public abstract class Vehicle {

    private static List<Vehicle> vehicleList = new ArrayList<>();
    public abstract void startEngine();

    public void addVehicleToList() {
        vehicleList.add(this);
    }

    public static void printVehicles (){
        System.out.println(vehicleList);
    }
}

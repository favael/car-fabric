package Exercise.data.vehicles.cars;

public enum Color {

    WHITE("White"),
    RED("Red"),
    BLACK("Black"),
    ORANGE("Orange");

   private String colorName;

    Color(String colorName) {
        this.colorName = colorName;
    }

    public String getColorName() {
        return colorName;
    }

    @Override
    public String toString() {
        return colorName;
    }
}

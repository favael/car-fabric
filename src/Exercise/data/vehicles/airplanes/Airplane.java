package Exercise.data.vehicles.airplanes;

import Exercise.data.vehicles.Vehicle;

public class Airplane extends Vehicle {
    private String name;
    public Airplane(String name) {
        this.name = name;
        addVehicleToList();
    }

    @Override
    public void startEngine() {
        System.out.println("Engine started, ready to fly!!");
    }

    @Override
    public String toString() {
        return "Airplane{" +
                "name='" + name + '\'' +
                '}';
    }
}

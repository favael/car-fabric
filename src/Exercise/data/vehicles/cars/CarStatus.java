package Exercise.data.vehicles.cars;

public enum CarStatus {
    DAMAGED(false),
    BRANDNEW(true);

    private boolean status;

    CarStatus(boolean status) {
        this.status = status;
    }
}

package Exercise.data.vehicles.ships;

import Exercise.data.vehicles.Vehicle;

public class Ship extends Vehicle {
    private String name;
    public Ship(String name) {
        this.name = name;
        addVehicleToList();
    }

    @Override
    public void startEngine() {
        System.out.println("Engine started, ready to swim!!");
    }

    @Override
    public String toString() {
        return "Ship{" +
                "name='" + name + '\'' +
                '}';
    }
}

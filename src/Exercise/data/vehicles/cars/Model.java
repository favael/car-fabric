package Exercise.data.vehicles.cars;

public class Model {
    private String modelName;
    private String modelVariant;
    private Gearbox gearbox;
    private int engineSize;
    private int enginePower;


    public Model(String modelName, String modelVariant, Gearbox gearbox, int engineSize, int enginePower) {
        this.modelName = modelName;
        this.modelVariant = modelVariant;
        this.gearbox = gearbox;
        this.engineSize = engineSize;
        this.enginePower = enginePower;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getModelVariant() {
        return modelVariant;
    }

    public void setModelVariant(String modelVariant) {
        this.modelVariant = modelVariant;
    }

    public Gearbox getGearbox() {
        return gearbox;
    }

    public void setGearbox(Gearbox gearbox) {
        this.gearbox = gearbox;
    }

    public int getEngineSize() {
        return engineSize;
    }

    public void setEngineSize(int engineSize) {
        this.engineSize = engineSize;
    }

    public int getEnginePower() {
        return enginePower;
    }

    public void setEnginePower(int enginePower) {
        this.enginePower = enginePower;
    }

    @Override
    public String toString() {
        return "Model{" +
                "modelName='" + modelName + '\'' +
                ", modelVariant='" + modelVariant + '\'' +
                ", gearbox=" + gearbox +
                ", engineSize=" + engineSize +
                ", enginePower=" + enginePower +
                '}';
    }
}




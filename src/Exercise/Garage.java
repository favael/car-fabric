package Exercise;

import Exercise.data.owners.Owner;
import Exercise.data.vehicles.Vehicle;
import Exercise.data.vehicles.airplanes.Airplane;
import Exercise.data.vehicles.cars.*;
import Exercise.data.vehicles.ships.Ship;

public class Garage {
    public static void main(String[] args) {

        Car bmwX6 = new Car(Brand.BMW, new Model("X6","M Sport", Gearbox.AUTOMAT,
                4000,450),0, CarStatus.BRANDNEW);
        Car toyotaYaris = new Car(Brand.TOYOTA, new Model("Yaris", "T4",
                Gearbox.MANUAL, 1200, 90), 0, CarStatus.BRANDNEW);
        Car audiA8 = new Car(Brand.AUDI,new Model("A8","W12", Gearbox.AUTOMAT,
                6000,500),0,CarStatus.BRANDNEW);
        Vehicle boeing = new Airplane("Boeing");
        Vehicle titanic = new Ship("Titanic");

        Owner rafal = new Owner("Rafał", "Dogoda");

        rafal.buyCar(toyotaYaris);
        rafal.buyCar(bmwX6);
        rafal.printCarList();

        Car.printCars();
        audiA8.startEngine();

        boeing.startEngine();
        titanic.startEngine();

        Vehicle.printVehicles();
    }

}








//        Suv suv = new Suv(Brand.TOYOTA, 5, 200000, Color.BLACK.getColorName(), 2);
//        SportCar bmw = new SportCar(Brand.BMW, 2, 5000, Color.RED.getColorName(), 400);
//        TransportCar mercedes = new TransportCar(Brand.MERCEDES, 5, 1000000, Color.WHITE.getColorName(), 3);
//        Cabrio cabrio = new Cabrio(Brand.OPEL,2,10000,Color.ORANGE.getColorName(), true );

        //w ramach cwiczen zmienilismy ArrayList na Queue

//        Queue <Car> cars = new ArrayDeque<>();
//
//        cars.offer(suv);
//        cars.offer(bmw);
//        cars.offer(mercedes);
//        cars.offer(cabrio);
//
//        // comment for Wlodek
//
//        System.out.println("cars.size() = " + cars.size());
//
//        int size = cars.size();
//
//        for (int i = 0; i < size; i++) {
//            System.out.println(cars.poll().startEngine() );
//
//        }
//
//        //dodajemy ponownie obiekty do kolejki, poniewaz cars.poll() pobiera i usuwa obiekty
//
//        cars.offer(suv);
//        cars.offer(bmw);
//        cars.offer(mercedes);
//        cars.offer(cabrio);
//
//        // metoda fix - w ramach cwiczen dodany interfejs(Service) z metodą implementowaną dla samochodów - różne
//        // funkcjonalnosci dla poszczególnych rodzajów aut
//
//        for (int i = 0; i < size; i++) {
//            System.out.println("cars.peek().fixMe() = " + cars.poll().fixMe());
//        }
//
//    }
//}
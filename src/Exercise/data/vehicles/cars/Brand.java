package Exercise.data.vehicles.cars;

public enum Brand {

    AUDI("Audi"),
    BMW("Bmw"),
    MERCEDES("Mercedes"),
    TOYOTA("Toyota"),
    OPEL("Opel");

   private String name;

    Brand(String name) {
        this.name = name;
    }

}

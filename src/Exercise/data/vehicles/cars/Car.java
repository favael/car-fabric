package Exercise.data.vehicles.cars;

import Exercise.data.owners.Owner;
import Exercise.data.vehicles.Vehicle;


import java.util.ArrayList;

public class Car extends Vehicle {
    private Brand brand;
    private Model model;
    private int millage;
    private CarStatus carStatus;

    private Owner owner;
    private static ArrayList<Car> carList = new ArrayList<>();




    public Car(Brand brand, Model model, int millage, CarStatus carStatus) {
        this.brand = brand;
        this.model = model;
        this.millage = millage;
        this.carStatus = carStatus;
        carList.add(this);
        addVehicleToList();
    }

    public Car(Brand brand, Model model, int millage, CarStatus carStatus, Owner owner) {
        this(brand, model, millage, carStatus);
        this.owner = owner;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public int getMillage() {
        return millage;
    }

    public void setMillage(int millage) {
        this.millage = millage;
    }

    public CarStatus getCarStatus() {
        return carStatus;
    }

    public void setCarStatus(CarStatus carStatus) {
        this.carStatus = carStatus;
    }

    public static void printCars () {
        if (carList.isEmpty()) {
            System.out.println("No cars available");
        } else {
            System.out.println(carList);
        }
    }

    @Override
    public String toString() {
        return "Car{" +
                "brand=" + brand +
                ", model=" + model +
                ", millage=" + millage +
                ", carStatus=" + carStatus +
                ", owner=" + owner +
                '}';
    }

    @Override
    public void startEngine() {
        System.out.println(getBrand().name() + " starting engine.");
    }
}



